import requests


def get_tweets_and_token(url_bbb, header):
    tweets = requests.get(url_bbb, headers=header)
    tweets = tweets.json()
    tweets_data = tweets['data']
    if 'next_token' in tweets['meta']:
        tweets_token = tweets['meta']['next_token']
    else:
        tweets_token = ''
    return tweets_data, tweets_token


def get_url():
    url_bbb = 'https://api.twitter.com/2/tweets/search/recent?query=BBB23&tweet.fields=author_id'
    header = {
        'Authorization': 'Bearer ' + 'AAAAAAAAAAAAAAAAAAAAAO9KlgEAAAAATf5RIcFd1EvI2n2iaPO%2Bwd3Riy8%3DWsmpNpa2x22BC12TEyiv0wNN1nmNzWsEK70gwrwHrtmfmGCmQn'}
    return url_bbb, header


def signal_create_participant(url_api, params, name):
    print(f'\033[31m' + name.upper() + '\033[0;0m')

    data = {
        "name": "Tweet",
        "version": "1.0.0",
        "metrics": {
            "count":
                {"value": 1,
                 "type": "number"
                 },
            "name":
                {"value": name,
                 "type": "string"}}}

    requests.post(url_api, params=params, json=data)


def find_name(list_tweet_text):
    participants_list = ['sapato', 'amanda', 'amandinha', 'mosca', 'alface', 'fred', 'antonio', 'bruna', 'guime',
                         'key', 'gabriel', 'santana', 'cristian', 'gustavo', '@choquei', 'domitila', 'paredao', 'anjo',
                         'lider', 'bbb', '#redebbb', 'paula', 'tina', 'cowboy']
    for word in list_tweet_text:
        if word in participants_list:
            return word
        else:
            return []


def signal_create(url_api, params):
    data = {
        "name": "Tweet",
        "version": "1.0.0",
        "metrics": {
            "count": {
                "value": 1,
                "type": "number"
            }
        }}
    requests.post(url_api, params=params, json=data)
