import requests
from time import sleep
import http.client
from Modules import *


params = {'app': input("Digite o UUID da aplicação: ")}
url_api = 'http://127.0.0.1:9100/api/analytics/signal/'
url_bbb, header = get_url()
tweets_token = ''
while True:
    try:
        tweets, tweets_token = get_tweets_and_token(url_bbb, header)

        tweets_token = '&next_token=' + tweets_token
        for tweet in tweets:
            tweet_text = tweet['text'].lower()
            list_tweet_text = tweet_text.split(' ')
            word = find_name(list_tweet_text)
            if word:
                signal_create_participant(url_api, params, word)
            else:
                signal_create(url_api, params)
    except Exception as e:
        print(e)
        print("Deu erro! Espere um instante!")
        sleep(15)
    sleep(5)
    url_bbb, old_token = url_bbb.split('&', 2)

    if old_token != tweets_token:
        url_bbb = url_bbb + tweets_token
    if tweets_token == '&next_token=':
        break
